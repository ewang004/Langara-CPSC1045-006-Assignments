//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function pointcreate(points, inner, outer){
    let degree = 360 / (2 * points)
    let array = [];
    let xVal, yVal;
    for (let i=0; i < 2 * points; i++){
        if (i%2 == 0){
        xVal = outer * Math.cos(degree * i);
        yVal = outer * Math.sin(degree * i);
        }
        else {
        xVal = inner * Math.cos(degree * i);
        yVal = inner * Math.sin(degree * i);
        }
        let p = {x:xVal, y:yVal};
        array.push(p);
        console.log("f1");
    }
    return array;
}

function pointString(arr){
    console.log("f2");
    let str= "";
    for (let i=0; i < arr.length; i++){
        str = str+ " " + arr[i].x + " " + arr[i].y;
    }
    return str;
}

function makeStar(){
    let numPoints = Number(document.querySelector("#num").value);
    let arr = pointcreate(numPoints, 100, 200);
    let svgString = pointString(arr);
    document.querySelector("#star").setAttribute("points" , svgString); 
    console.log("f3")
}

window.makeStar = makeStar;