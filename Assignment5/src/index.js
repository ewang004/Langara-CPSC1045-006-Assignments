//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
/*jshint esversion: 6 */ 
console.log("Hello world");

function E3Q2(minN , maxN , n){
    if(minN < n && n < maxN)
        console.log("Part3");
}

let count = 0;

function answerAll(){
    count = 0; //reset 
    easy( "#q1", 10, 0);
    easy( "#q2", 7, 1);
    easy( "#q3", 15, 2);
    easy( "#q4", 20, 3);
    easy( "#q5", 1, 4);
  
    document.querySelector("#numCount").innerHTML = "# of correct answers " + count;
}

function easy(id, answer, question) {
    // if answer is correct circle turns green
    // If answer not correct circle stays red
    let ans = Number(document.querySelector(id).value);
    console.log(ans);
    let circles = document.querySelectorAll(".yes");
    console.log(circles);
    if (ans == answer) {
        circles[question].setAttribute("fill" , "green");
        count = count + 1;
    }
    else { 
        circles[question].setAttribute("fill" , "red");
    }
}