//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.\
/*jshint esversion: 6 */
console.log("Blocky meets circle");

function update() {
let rotation = document.querySelector("#sliderxy").value;
var collection = document.getElementById("#collection");
let rotateString = "rotate(" +rotation+ " 50 50)";
console.log(rotateString);
collection.setAttribute("transform" , rotateString);
let xnumberbox = document.querySelector("x");
let ynumberbox = document.querySelector("y");
console.log(xnumberbox);
console.log(ynumberbox);
let translateString = "translate(" + xnumberbox.value + " "+ ynumberbox.value + ")";
console.log(translateString);
let transformString = translateString + " " + rotateString;
console.log(transformString);
collection.setAttribute("transform", transformString);

}

window.update = update;

function change(){
    //get all the elements we need from HTML 
    let inner = document.querySelector("#ir").value;
    let outer = document.querySelector("#or").value;
    let poly = document.querySelector("#poly");

    console.log(inner);
    console.log(outer);

    //calculate 16 points based on inner and outer radii
    let p1y = outer * Math.sin(0);  
    let p1x = outer * Math.cos(0);
    let p2x = inner * Math.cos(0.76);
    let p2y = inner * Math.sin(0.76);
    let p3x = outer * Math.cos(1.57);  
    let p3y = outer * Math.sin(1.57);
    let p4x = inner * Math.cos(2.36);
    let p4y = inner * Math.sin(2.36);
    let p5x = outer * Math.cos(3.14);
    let p5y = outer * Math.sin(3.14);
    let p6x = inner * Math.cos(3.92);
    let p6y = inner * Math.sin(3.92);
    let p7x = outer * Math.cos(4.71);
    let p7y = outer * Math.sin(4.71);
    let p8x = inner * Math.cos(5.50);
    let p8y = inner * Math.sin(5.50);

    //making a new string with our new points 
    // let pointString = "" + p1x +" "+ p1y +" "+ p2x +" "+ p2y +" "+p3x +" "+ p3y +" "+ p4x +" "+ p4y +" "+ p5x +" "+ p5y +" "+ p6x +" "+p6y +" "+ p7x +" "+ p7y +" "+ p8x +" "+ p8y;
    let pointString = p1x +" "+ p1y +" "+ p2x +" "+ p2y +" "+p3x +" "+ p3y +" "+ p4x +" "+ p4y +" "+ p5x +" "+ p5y +" "+ p6x +" "+p6y +" "+ p7x +" "+ p7y +" "+ p8x +" "+ p8y;
    
    console.log(pointString);   

    //put the new string into the points attribute of the poly 
    poly.setAttribute("points", pointString);

    let x = document.querySelector("#x2").value;
    let y = document.querySelector("#y2").value;
    console.log(document.querySelector("#x"));
    console.log(y);
    let translateString = "" + "translate(" +x +" "+ y +")";
    let g = document.querySelector("#polygon");
    console.log(translateString);
    console.log(g);
    g.setAttribute("transform",translateString);

}

//export a function to windows 
window.change = change;
//call change in the very beginning (onload)
window.onload = change;


